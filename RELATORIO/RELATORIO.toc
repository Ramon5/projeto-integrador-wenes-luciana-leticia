\changetocdepth {4}
\select@language {brazil}
\select@language {brazil}
\contentsline {chapter}{Sum\'ario}{7}{section*.5}
\contentsline {chapter}{Introdu\IeC {\c c}\IeC {\~a}o}{8}{chapter*.6}
\contentsline {chapter}{\chapternumberline {1}Objetivos}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Geral}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Espec\IeC {\'\i }fico}{9}{section.1.2}
\contentsline {chapter}{\chapternumberline {2}Justificativa}{10}{chapter.2}
\contentsline {chapter}{\chapternumberline {3}Descri\IeC {\c c}\IeC {\~a}o do Sistema}{11}{chapter.3}
\contentsline {chapter}{\chapternumberline {4}Plano de Marketing}{12}{chapter.4}
\contentsline {chapter}{\chapternumberline {5}Requisitos}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Requisitos Funcionais}{13}{section.5.1}
\contentsline {section}{\numberline {5.2}Requisitos N\IeC {\~a}o Funcionais}{13}{section.5.2}
\contentsline {chapter}{\chapternumberline {6}Detalhamento de Caso de Uso}{14}{chapter.6}
\contentsline {chapter}{\chapternumberline {7}Vis\IeC {\~a}o Funcional}{18}{chapter.7}
\contentsline {section}{\numberline {7.1}Diagrama de Caso de Uso}{18}{section.7.1}
\contentsline {section}{\numberline {7.2}Diagrama de Classe}{19}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Atributos}{19}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}M\IeC {\'e}todos}{20}{subsection.7.2.2}
\contentsline {chapter}{\chapternumberline {8}Considera\IeC {\c c}\IeC {\~o}es Finais}{21}{chapter.8}
\contentsline {chapter}{Refer\^encias}{22}{section*.8}
